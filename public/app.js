angular.module('app', []).controller('accounts', ['$scope', '$http', 'socket', function($scope, $http, socket) {
  $scope.title = 'SMURFEZ BULK PROFILE UPDATER';
  $scope.name;
  $scope.password;
  $scope.editname;
  $scope.editpassword;
  $scope.index;
  $scope.accounts = [];
  $scope.config;
  $scope.status = [];
  let count = 0;
  $http.get('/read/accounts').then(function(res) {
    $scope.accounts = res.data;
  });
  $http.get('/read/config').then(function(res) {
    $scope.config = res.data;
  });
  $scope.pushaccount = function() {
    $scope.accounts.push({
      name: $scope.name,
      password: $scope.password
    });
    $http.post('/update/accounts', $scope.accounts).then(function(body){
      console.log(body.data);
    });
    $scope.name = undefined;
    $scope.password = undefined;
  };
  $scope.selectEntry = function(i) {
    $scope.editname = $scope.accounts[i].name;
    $scope.editpassword = $scope.accounts[i].password;
    $scope.index = i;
  };
  $scope.updateEntry = function() {
    $scope.accounts[$scope.index].name = $scope.editname;
    $scope.accounts[$scope.index].password = $scope.editpassword;
    $http.post('/update/accounts', $scope.accounts).then(function(body){
      console.log(body.data);
    });
  };
  $scope.deleteEntry = function(i) {
    $scope.accounts.splice(i, 1);
    $http.post('/update/accounts', $scope.accounts).then(function(body){
      console.log(body.data);
    });
  };
  $scope.start = function() {
    count = 0;
    $scope.status = [];
    socket.emit('start', $scope.accounts);
  };
  socket.on('status', function(data) {
    count++;
    $scope.status.push(count+'. '+data);
  });
}]).factory('socket', function($rootScope) {
  var socket = io.connect();
  return {
    on: function(eventName, callback) {
      socket.on(eventName, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          callback.apply(socket, args);
        });
      });
    },
    emit: function(eventName, data, callback) {
      socket.emit(eventName, data, function() {
        var args = arguments;
        $rootScope.$apply(function() {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});;
