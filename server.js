const express = require('express');
const bodyParser = require('body-parser')
const fs = require('fs');

const port = process.env.PORT || 8080;

const steam = require('./steam');

const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

app.get('/read/:file', async (req, res) => {
  fs.readFile('./' + req.params.file + '.json', (err, data)=>{
    if(err) res.send({error: err});
    else res.send(data);
  });
});

app.post('/update/:file', async (req, res) => {
  fs.writeFile('./' + req.params.file + '.json', JSON.stringify(req.body), err => {
    if (err) res.send({
      error: err
    });
    else res.send({
      message: 'update success'
    });
  });
});

io.on('connection', function(socket){
  socket.on('start', async (data)=>{
    fs.writeFile('./accounts.json', JSON.stringify(data), err => {
      if(err) socket.emit('status', 'err: '+err);
      else steam(data, require('./config.json'), socket);
    });
  });
});

http.listen(port, err => {
  if (err) throw err;
  console.log('server online at http://localhost:' + port);
});
