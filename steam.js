const steam = require('steamcommunity');

module.exports = (accounts, config, socket) => {
  accounts.forEach(account => {
    let acc = new steam();
    acc.login({
      accountName: account.name,
      password: account.password
    }, function(err, ses) {
      if (err) socket.emit('status', account.name + ' account update unsuccesful due to $' + err);
      else {
        let id = acc.steamID.getSteamID64();
        socket.emit('status', '['+id+'] Logged in successfully');
        acc.setupProfile(err => {
          if (err) {
            socket.emit('status', '['+id+'] account update unsuccesful due to $' + err);
          } else acc.editProfile({
            name: config.name,
            summary: config.summary
          }, err => {
            if (err) socket.emit('status', '['+id+'] account update unsuccesful due to $' + err);
            else socket.emit('status', '['+id+'] account update succesful');
            acc.getSteamGroup(config.group, function(err, group) {
              if (err) socket.emit('status', '['+id+'] group update unsuccesful due to $' + err);
              else group.join(err => {
                if (err) socket.emit('status', '['+id+'] group update unsuccesful due to $' + err);
                else socket.emit('status', '['+id+'] group update succesful');
              });
            });
            acc.uploadAvatar(config.avatar, err => {
              if (err) socket.emit('status', '['+id+'] avatar update unsuccesful due to $' + err);
              else socket.emit('status', '['+id+'] avatar update successful');
            });
            acc.profileSettings(config.privacy, err => {
              if (err) socket.emit('status', '['+id+'] privacy setting update unsuccesful due to $' + err);
              else socket.emit('status', '['+id+'] privacy setting update succesful');
            });
          });
        });
      }
    });
  });
}
